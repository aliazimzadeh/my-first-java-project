package com.example.lib;

import java.util.List;

public class Factory {

    public Factory(){

    }

    private Employee manager;
    private List<Employee> employees;
    private  String name;

    public Employee getManager() {
        return manager;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public String getName() {
        return name;
    }

    public void setEmployees(List<Employee> value) {
        employees = value;
    }

    public void setManager(Employee value) {
        manager = value;
    }

    public void setName(String value) {
        this.name = value;
    }
}
