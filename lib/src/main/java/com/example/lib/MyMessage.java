package com.example.lib;

public class MyMessage {

    public  static  void DisplayError(String errorMessage){

        System.out.println(errorMessage);
    }

    public static void ShowMessage(String hint, String message){

        String strMessage;
        strMessage=String.format("%s is %s",hint,message);
        System.out.println(strMessage);

    }

    public static void ShowMessage(String message){

        System.out.println(message);

    }
}
