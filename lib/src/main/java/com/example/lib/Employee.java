package com.example.lib;

public class Employee extends Person{

    public Employee(){

    }

    private  boolean isManager;
    private  int salary;
    private  int emploeeId;

    public boolean isManager() {
        return isManager;
    }

    public int getEmploeeId() {
        return emploeeId;
    }

    public int getSalary() {
        return salary;
    }

    public void setEmploeeId(int value) {
        this.emploeeId = value;
    }

    public void setManager(boolean value) {
        if(value == true && getAge()<40){
            value = false;
            MyMessage.DisplayError("this age is not valid for manager!");
        }
        isManager = value;
    }

    public void setSalary(int value) {
        this.salary = value;
    }


}
