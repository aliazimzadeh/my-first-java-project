package com.example.lib;

import java.util.ArrayList;
import java.util.List;

public class MyClass {
    public  static  void  main(String[] arg)
    {
       // System.out.print("Hello World!");
        DoSomethings();
    }

    private static void DoSomethings() {

//        //Create an instance of person class and set the properties
//        Person oPerson = new Person();
//        oPerson.setFirstName("Ali");
//        oPerson.setLastName("Azimzadeh");
//        oPerson.setAge(40);
//
//        Person oPerson1 = new Person();
//        oPerson1.setFirstName("Reza");
//        oPerson1.setLastName("Abassi");
//        oPerson1.setAge(25);
//
//        Person oPerson2 = new Person();
//        oPerson2.setFirstName("Nina");
//        oPerson2.setLastName("Yousefi");
//        oPerson2.setAge(35);


        //Create an instance of employee class and set the properties
        Employee oEmployee = new Employee();
        oEmployee.setFirstName("Ali");
        oEmployee.setLastName("Azimzadeh");
        oEmployee.setAge(40);
        oEmployee.setEmploeeId(100);
        oEmployee.setManager(true);
        oEmployee.setSalary(100000);


        Employee oEmployee1 = new Employee();
        oEmployee1.setFirstName("Reza");
        oEmployee1.setLastName("Abassi");
        oEmployee1.setAge(25);
        oEmployee1.setEmploeeId(200);
        oEmployee1.setManager(true);
        oEmployee1.setSalary(200000);

        Employee oEmployee2 = new Employee();
        oEmployee2.setFirstName("Nina");
        oEmployee2.setLastName("Yousefi");
        oEmployee2.setAge(35);
        oEmployee2.setEmploeeId(300);
        oEmployee2.setManager(false);
        oEmployee2.setSalary(300000);


        List<Employee> employees = new ArrayList<>();
        employees.add(oEmployee);
        employees.add(oEmployee1);
        employees.add(oEmployee2);


        //Create an instance of factory class and set the properties
        Factory oFactory = new Factory();
        oFactory.setName("Apna");
        oFactory.setEmployees(employees);
        oFactory.setManager(oEmployee);

        MyMessage.ShowMessage("FactoryName",oFactory.getName());
        MyMessage.ShowMessage("Employees Info is :");

        for (Employee item:oFactory.getEmployees()) {
            MyMessage.ShowMessage("FirstName",item.getFirstName());
            MyMessage.ShowMessage("LastName", item.getLastName());
            MyMessage.ShowMessage("Age",item.getAge()+"");
            MyMessage.ShowMessage("EmployeeId",item.getEmploeeId()+"");
            MyMessage.ShowMessage("Salary",item.getSalary()+"");
            MyMessage.ShowMessage("IsManager",item.isManager()+"");
            MyMessage.ShowMessage("///////////////////////////////////////////////////");

        }
        MyMessage.ShowMessage("FactoryManager",oFactory.getManager().getFirstName()+ " "+oFactory.getManager().getLastName());


    }
}
